const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require("nodemailer");

const app = express();

const PORT = 3000

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.post("/", (req, res) => {

    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'maureen.reichert74@ethereal.email',
            pass: 'arxcPpHEYR5BsfFDw6'
        }
    });

    let message = {
        from: 'sender@example.com',
        to: req.body.to,
        subject: 'Hi',
        text: req.body.email_body,
    };

    transporter.sendMail(message, (err, info) => {
        if(err){
            console.log(err)
            return res.status(500).json({
                "success" : false,
                "message" : err
            })
        }

        console.log(info)
        return res.status(200).json({
            "success" : true,
            "message" : "Email sent successfully"
        })
    })

});


app.listen(process.env.PORT || PORT, function(){
    console.log(`Server started at port ${PORT}`);
});